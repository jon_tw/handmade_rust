# handmade_rust

Base tutorial I used:
    https://www.jendrikillner.com/post/rust-game-part-2/

References & Influences:
```
1. Casey Muratori (Handmade Hero):
    - https://handmadehero.org/
    - https://twitter.com/cmuratori
    - https://www.youtube.com/c/MollyRocket
2. Ryan Fleury:
    - https://twitter.com/ryanjfleury
3. Ryan Ries (Win32 Game from Scrath youtube series):
    - https://www.youtube.com/user/ryanries09
```

Extra-Info:
```
reg3d sound library uses directsound for windows.
I couldn't be bothered to try and work with that myself.
```

Dependencies (Cargo.toml):
```
winapi = { version = "0.3", features = ["winuser"] }
kernel32-sys = "0.2.2"
rusty-xinput = "1.2.0"
libc = "0.2.*"
rg3d-sound = "0.8.0"
```
