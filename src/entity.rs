use crate::{
    math::{Color, Point, Rect, V2f},
    win32_engine::{Win32Drawable, Win32Engine},
};

// IDEA(S):
/*
  Entity Component System?
  I really don't want to have every optional
  characterisistic that an entity could have
  all in the entity struct, rather
  just optional components that can be enabled.
*/

// Perhaps figure out render order before doing
// an entity component system?

pub enum EntityType {
    RECT,
}

pub struct Entity {
    pos: V2f,
    size: V2f,
    ent_type: EntityType,
    vel: Point<f32>,
    color: Color,
}

impl Entity {
    pub fn new(pos: V2f, ent_type: EntityType) -> Self {
        Self {
            pos,
            size: V2f::new(64.0, 64.0),
            ent_type,
            vel: Point::new(150.0, 150.0),
            color: Color::new(0, 0, 0, 0),
        }
    }

    pub fn input(&mut self, engine: &mut Win32Engine) {
        // Only process input if the game window has focus
        // Ideally we don't want to have to have this here, but rn it doesn't matter all that much
        if engine.check_focus() {
            // Input

            // Digital movement tuning
            let mut d_x = 0.0; // essentially pixels/sec
            let mut d_y = 0.0;

            if engine.input.up() {
                d_y = -1.0;
            }
            if engine.input.down() {
                d_y = 1.0;
            }
            if engine.input.left() {
                d_x = -1.0;
            }
            if engine.input.right() {
                d_x = 1.0;
            }

            // Multiply our movement variables by the velocity we want
            d_x *= self.vel.x;
            d_y *= self.vel.y;

            // If moving diagnal multiply by sqrt of 1/2
            if d_x != 0.0 && d_y != 0.0 {
                d_x *= 0.707106781186547;
                d_y *= 0.707106781186547;
            }

            // have to multiply movement speed by dt so movement is consistent
            // no matter how fast or slow the software is running

            self.pos.x += engine.time_manager.get_time_step() * d_x;
            self.pos.y += engine.time_manager.get_time_step() * d_y;
        }
    }

    pub fn update(&mut self, engine: &mut Win32Engine) {
        // Only allow input for player
        // entity type
        match self.ent_type {
            EntityType::RECT => {
                self.input(engine);
            }
        }

        // Screen collision
        if self.pos.x >= (engine.get_width() as f32 - self.size.x) {
            self.pos.x = engine.get_width() as f32 - self.size.x - 1.0;
        }
        if self.pos.x <= 1.0 {
            self.pos.x = 1.0;
        }
        if self.pos.y >= (engine.get_height() as f32 - self.size.y) {
            self.pos.y = engine.get_height() as f32 - self.size.y - 1.0
        }
        if self.pos.y <= 1.0 {
            self.pos.y = 1.0;
        }
    }

    pub fn draw(&self, engine: &Win32Engine) {
        match self.ent_type {
            EntityType::RECT => {
                let rect = Rect::new(self.pos.x, self.pos.y, self.size.x, self.size.y);
                engine.draw_rectangle(self.color, rect)
            }
        }
    }
}
