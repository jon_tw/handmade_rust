use crate::{entity::Entity, win32_engine::Win32Engine};

/*
 * NOTE: (12/2/21)
 * I have no idea if this way of managing entities is good. :Shrug:
 *
 * TODO:
 * Research entity things.
*/

pub struct EntityManager {
    entities: Vec<Entity>,
}

impl EntityManager {
    pub fn new() -> Self {
        Self {
            entities: Vec::new(),
        }
    }

    pub fn create(&mut self, entity: Entity) {
        self.entities.push(entity);
    }

    pub fn update(&mut self, engine: &mut Win32Engine) {
        for entity in &mut self.entities {
            entity.update(engine);
        }
    }

    pub fn draw(&self, engine: &Win32Engine) {
        for entity in &self.entities {
            entity.draw(engine);
        }
    }
}
